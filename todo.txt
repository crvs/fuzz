interactive interface (NCurses?)
use the fuzz-score function and qsort(2) to sort the matches
write unit tests
gnu autotools

# ongoing notes

- CUnit for unit tests

# done
gracefully refuse to open non-regular/non-existing files intead of sefaulting - stat(2)
FIX segfault associated to improper line reading - wrong use of getline(2)
FIX segfault in handling multiple files - deleting out of bounds
check file permissions when opening - stest in dmenu gives an idea on file permissions
checkout fuzzy matching in dmenu - it does exact matching
