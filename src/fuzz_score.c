#include <stdio.h>

/// @param str
///    null terminated string
/// @returns length of the string
int len(char* str)
{
    int l = 0;
    for (; str[l] != 0; ++l) {}
    return l;
}

/// @param pattern
/// @param pattern_length
/// @param string
/// @param string_length
/// @param last_match
int fuzz_score_rec(char* pattern,
              int pattern_length,
              char* string,
              int string_length,
              int last_match,
              int cumulative_score)
{
    if (pattern_length == 0)
    {
        return cumulative_score;
    }
    else if (string_length == 0)
    {
        return 0;
    }
    else if (pattern[0] == string[0])
    {
        if (last_match == 0)
        {
            fuzz_score_rec(pattern + 1,
                      pattern_length - 1,
                      string + 1,
                      string_length - 1,
                      0,
                      cumulative_score + 2);
        }
        else
        {
            fuzz_score_rec(pattern + 1,
                      pattern_length - 1,
                      string + 1,
                      string_length - 1,
                      0,
                      cumulative_score + 1);
        }
    }
    else
    {
        fuzz_score_rec(pattern, pattern_length, string + 1, string_length - 1, 0, cumulative_score);
    }
}

/// @param pattern
///    null terminated string with the pattern that is to be matched/searched
/// @param string
///    null terminated string with the string that is to be matched against the pattern
int fuzz_score(char* pattern, char* string)
{
    // initialize recursion
    return fuzz_score_rec(pattern, len(pattern), string, len(string), 0, 1);
}

struct scored_string
{
    char* string;
    int score;
};

int main(int argc, char **argv)
{
    char* pattern = argv[1];
    for (int i = 2; i < argc; ++i)
    {
        int score = fuzz_score(pattern, argv[i]);
        if (score > 0)
        {
            printf("%s\n",argv[i]);
        }
    }
    return 0;
}

