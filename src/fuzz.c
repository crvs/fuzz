#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "fuzz.h"

int main(int argc, char** argv)
{

    filtt_t filtt[1];
    FILE* input;

    for (size_t i = 2; i < argc; ++i)
    {
        new_filtered_text(filtt);

        set_pattern(filtt, argv[1]);
        // extend_pattern(filtt, argv[1]);

        struct stat exists[1];
        if (stat(argv[i], exists) != -1     /*exists*/
            && S_ISREG(exists->st_mode)     /*is regular file*/
            && access(argv[i], R_OK) == 0)  /*is readable*/
        {
            input = fopen(argv[i] , "r");

            printf("\nMatches to '%s' in '%s':\n", argv[1], argv[i]);
            filter_file(filtt, input);
            print_filtered(filtt);

            clear_filtered_text(filtt);

            fclose(input);
        }
        else
        {
            fprintf(stderr, "not a regular file: %s\n", argv[i]);
        }
    }
    return 0;
}
