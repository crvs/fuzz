#ifndef FUZZ_FILTER_H
#define FUZZ_FILTER_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#ifndef NDEBUG
#define MARKER printf("%s : %d\n", __FILE__ , __LINE__ ); fflush(stdout)
#else
#define MARKER
#endif

struct filtered_text
{
    /* pattern being matched */
    char* pattern;
    size_t pattern_size;

    /* input content */
    size_t n_lines;   /* number of lines*/
    char** lines;     /* line content */
    size_t* line_len; /* length of each line */

    /* match content */
    size_t* string_match_end;   /* last element of string to match */
    size_t* pattern_match_end;  /* last element of pattern to match */

    // the pattern is considered matched in line i if:
    // $ pattern_match_end[i] = pattern_size - 1

    /* dynamic alloction accounting */

    /* reallocation parameters for this->pattern */
    size_t _pattern_max_size;        // maxium pattern size.
    size_t _pattern_max_increment;   // increment in #chars to maximum pattern size.

    /* reallocation parameters for this->lines */
    size_t _max_lines;           // maximum number of lines
    size_t _max_line_increment;  // increment in #lines to maxiumum line array.
};

typedef struct filtered_text filtt_t;

/* create the filtered text data structure */
int new_filtered_text(filtt_t* filtt);

/* set the pattern of the filtered text data structure */
int set_pattern(filtt_t* filtt, char* pattern);

/* extend the pattern by appending the given suffix */
int extend_pattern(filtt_t* filtt, char* suffix);

/* run the pattern on a the line-th line of the input */
int run_pattern_line(filtt_t* filtt, size_t line);

/* add the first line from the file descriptor to the input and filter it */
int filter_new_line(filtt_t* filtt, FILE* fd);

/* add every line of the file descriptor to the filtered_text */
int filter_file(filtt_t* filtt, FILE* fd);

/* output the filtered text to the console*/
int print_filtered(filtt_t* filtt);


int new_filtered_text(filtt_t* filtt)
{
    size_t max_lines = 255;
    size_t max_line_increment = 50;

    size_t pattern_max_size = 255;
    size_t pattern_max_increment = 50;

    /* essentially const parameters */
    filtt->_pattern_max_size = pattern_max_size;
    filtt->_pattern_max_increment = pattern_max_increment;

    filtt->_max_lines = max_lines;
    filtt->_max_line_increment = max_line_increment;

    /* pattern related parameters */
    filtt->pattern = malloc(filtt->_pattern_max_size * sizeof(char));
    filtt->pattern_size = 0;

    /* line related parametres */
    filtt->lines = malloc(filtt->_max_lines * sizeof(char*));
    filtt->n_lines = 0;
    filtt->line_len = malloc(filtt->_max_lines * sizeof(size_t));

    /* match related parameters */
    filtt->string_match_end = malloc(filtt->_max_lines * sizeof(size_t));
    filtt->pattern_match_end = malloc(filtt->_max_lines * sizeof(size_t));
    return 0;
}

int clear_filtered_text(filtt_t* filtt)
{
    free(filtt->pattern);
    free(filtt->line_len);
    free(filtt->string_match_end);
    free(filtt->pattern_match_end);
    for (size_t i = 0; i < filtt->n_lines; ++i)
    {
        free(filtt->lines[i]);
    }
    free(filtt->lines);
    return 0;
}

int run_pattern_line(filtt_t* filtt, size_t line)
{
    size_t i = 0;
    size_t j = 0;
    while (i < filtt->pattern_size && j < filtt->line_len[line])
    {
        if (filtt->pattern[i] == filtt->lines[line][j])
        {
            ++i;
        }
        ++j;
    }
    filtt->pattern_match_end[line]  = i;
    filtt->string_match_end[line]  = j;
//    printf("%ld %ld %ld %ld \n",line,i,j, filtt->pattern_size);

    return 0;
}

int filter_new_line(filtt_t* filtt, FILE* fd)
{
    if (filtt->n_lines == filtt->_max_lines)
    {
        // extend
        size_t new_max = filtt->_max_lines + filtt->_max_line_increment;
        filtt->_max_lines += filtt->_max_line_increment;
        filtt->lines = realloc(filtt->lines, new_max * sizeof(char*));
        filtt->line_len = realloc(filtt->line_len, new_max * sizeof(char*));
        filtt->string_match_end = realloc(filtt->string_match_end, new_max * sizeof(size_t));
        filtt->pattern_match_end = realloc(filtt->pattern_match_end, new_max * sizeof(size_t));
    }

    size_t i = filtt->n_lines++;
    char** line_ptr = malloc(sizeof(char*));
    *line_ptr = NULL;
    size_t line_len = 0;
    MARKER;
    getline(line_ptr, &line_len, fd);
    MARKER;

    filtt->lines[i] = *line_ptr;
    MARKER;

    filtt->line_len[i] = strlen(*line_ptr);
    MARKER;

    run_pattern_line(filtt, i);
    return 0;
}

int filter_file(filtt_t* filtt, FILE* fd)
{
    size_t i = 0;
    while (feof(fd) == 0)
    {
        // printf("#%lu :", i++); MARKER;
        filter_new_line(filtt, fd);
    }
    return 0;
}

int print_filtered(filtt_t* filtt)
{
    // TODO implement
    for (size_t i = 0; i < filtt->n_lines; ++i)
    {

        if (filtt->pattern_match_end[i] == filtt->pattern_size)
        {
#ifdef DEBUG
            printf("SHOWN  %03ld         %s", i, filtt->lines[i]);
#else
            printf("%s", filtt->lines[i]);
#endif
        }
#ifdef DEBUG
        else
        {
            printf("HIDDEN %03ld %03ld %03ld %s",
                    i,
                    filtt->pattern_match_end[i],
                    filtt->pattern_size,
                    filtt->lines[i]);
        }
#endif
    }
    return 0;
}

int set_pattern(filtt_t* filtt, char* pattern)
{
    size_t pattern_l = strlen(pattern);
    filtt->pattern_size = 0;
    if (filtt->_pattern_max_size < pattern_l)
    {
        filtt->_pattern_max_size = filtt->_pattern_max_size *
            (pattern_l / filtt->_pattern_max_size);
        filtt->pattern = realloc(filtt->pattern,
                                 filtt->_pattern_max_size * sizeof(char));
    }

    for (size_t i = 0; pattern[i] != 0; ++i)
    {
        filtt->pattern[i] = pattern[i];
        filtt->pattern_size++;
    }

    /* clear the remainder of the pattern*/
    for (size_t i = filtt->pattern_size; i < filtt->_pattern_max_size; ++i)
    {
        filtt->pattern[i] = 0;
    }

    return 0;
}

int extend_pattern(filtt_t* filtt, char* suffix)
{

    size_t pattern_l = strlen(filtt->pattern) + strlen(suffix);
    if (filtt->_pattern_max_size < pattern_l)
    {
        filtt->_pattern_max_size = filtt->_pattern_max_size *
            (pattern_l / filtt->_pattern_max_size);
        filtt->pattern = realloc(filtt->pattern,
                                 filtt->_pattern_max_size * sizeof(char));
    }

    for (size_t i = 0; suffix[i] != 0; ++i)
    {
        filtt->pattern[filtt->pattern_size] = suffix[i];
        filtt->pattern_size++;
    }

    return 0;
}

#endif  // FUZZ_FILTER_H
