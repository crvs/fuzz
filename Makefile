CFLAGS=-DNDEBUG

all: bin/fuzz

bin/fuzz: bin src/fuzz.h src/fuzz.c
	cc src/fuzz.c ${CFLAGS} -ggdb -I src -o bin/fuzz

bin:
	@mkdir -p bin
